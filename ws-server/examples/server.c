#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "ws_server.h"
#include "wchar.h"
#include "../../inc/bdaqctrl.h"
#include "../../inc/compatibility.h"

#define deviceDescription L"MIC-1810,BID#15"
const wchar_t* profilePath = L"../../profile/DemoDevice.xml";
#define startChannel  2
#define sectionLength 10000 //5000
#define sectionCount  0
#define clockRate	  50000
#define channelCount  3

#define USER_BUFFER_SIZE  channelCount*sectionLength
double  Data[USER_BUFFER_SIZE];
int32   returnedCount = 0;

void BDAQCALL OnDataReadyEvent         (void * sender, BfdAiEventArgs * args, void *userParam);
void BDAQCALL OnOverRunEvent           (void * sender, BfdAiEventArgs * args, void *userParam);
void BDAQCALL OnCacheOverflowEvent     (void * sender, BfdAiEventArgs * args, void *userParam);
void BDAQCALL OnStoppedEvent           (void * sender, BfdAiEventArgs * args, void *userParam);

struct Server
{
    struct WSServer * ws;
    int opened;
};

void on_open_cb (void * pointer, const char * src)
{
    struct Server * server = (struct Server *) pointer;
    server->opened = 1;
    printf("WS Open: %s\n", src);
}

void on_error_cb (void * pointer, const char * src)
{
    struct Server * server = (struct Server *) pointer;
    server->opened = 0;
    printf("WS client error: %s\n", src);
}

void on_message_cb (void * pointer, const char * src)
{
    struct Server * server = (struct Server *) pointer;
    // printf("on_message: %s\n", src);
    // 回傳相同的訊息給 client
    server_broadcaster((*server).ws, src);
}

void waitAnyKey()
{
   do {SLEEP(1);} while (!kbhit());
}

int main(int argc, char* argv[])
{

    struct Server server;
    server.opened =0;

    // New WS server
    server.ws = server_new(10, 3013);
    // Set on_open callback
    server_set_on_open_cb(server.ws, (void *) &server, on_open_cb);
    // Set on_error callback
    server_set_on_error_cb(server.ws, (void *) &server, on_error_cb);
    // Set on_message callback
    // server_set_on_message_cb(server.ws, (void *) &server, on_message_cb);
    
    // Server listen
    printf("Server Listen: %d\n",server_listen(server.ws));
    
//The function is used to deal with 'DataReady' Event.
void BDAQCALL OnDataReadyEvent (void * sender, BfdAiEventArgs * args, void *userParam)
{
    int i = 0;
    int j = 0;
    int displayCount = 0;
    int getDataCount = 0;

    WaveformAiCtrl * waveformAiCtrl = NULL;
    waveformAiCtrl = (WaveformAiCtrl *)sender;
    getDataCount = MinValue(USER_BUFFER_SIZE, args->Count);
    WaveformAiCtrl_GetDataF64(waveformAiCtrl, getDataCount, Data, 0, &returnedCount, NULL, NULL, NULL);
	// printf("Streaming AI get data count is  %d\n", returnedCount);
	// printf("the first sample for each Channel are:\n");
    char sendMsgBuf[80];
    char strBuf[80];
    char dataBuf[channelCount][sectionLength*10];
    char sendMsg[channelCount*sectionLength*10];
    for(j = 0; j < channelCount; ++j)
    {
        sprintf(strBuf, "\"ch%d\":[",j);
        strcpy(dataBuf[j], strBuf);
    }
    for(i = 0; i < sectionLength; ++i) 
    {
        for(j = 0; j < channelCount; ++j)
        {
                sprintf(sendMsgBuf, "%6.3f,",Data[displayCount]);
                strcat(dataBuf[j], sendMsgBuf);
                displayCount++;
        }
    }
    for(j = 0; j < channelCount; ++j)
    {
        dataBuf[j][strlen(dataBuf[j])-1]='\0';
        strcat(dataBuf[j], "]");
    }
    strcpy(sendMsg, "VI_Data.json|{");
    strcat(sendMsg, dataBuf[0]);
    for(j = 1; j < channelCount; ++j)
    {
        strcat(sendMsg, ",");
        strcat(sendMsg, dataBuf[j]);
    }
    strcat(sendMsg, "}");
    server_broadcaster(server.ws, sendMsg);
}

    ErrorCode ret = Success;
    Conversion * conversion = NULL;
    Record * record = NULL; 
    wchar_t enumString[256];
    // Step 1: Create a 'WaveformAiCtrl' for Waveform AI function.
    WaveformAiCtrl * wfAiCtrl = WaveformAiCtrl_Create();
    
    // Step 2: Set the notification event Handler by which we can known the state of operation effectively.
    WaveformAiCtrl_addDataReadyHandler     (wfAiCtrl, OnDataReadyEvent,         NULL);
    WaveformAiCtrl_addOverrunHandler       (wfAiCtrl, OnOverRunEvent,           NULL);
    WaveformAiCtrl_addCacheOverflowHandler (wfAiCtrl, OnCacheOverflowEvent,     NULL);
    WaveformAiCtrl_addStoppedHandler       (wfAiCtrl, OnStoppedEvent,           NULL);

    do 
    {
        // Step 3: Select a device by device number or device description and specify the access mode.
        // in this example we use ModeWrite mode so that we can fully control the device, including configuring, sampling, etc.
        DeviceInformation devInfo;
        devInfo.DeviceNumber = -1;
        devInfo.DeviceMode   = ModeWrite;
        devInfo.ModuleIndex  = 0;
        wcscpy(devInfo.Description, deviceDescription);
        ret = WaveformAiCtrl_setSelectedDevice(wfAiCtrl, &devInfo);
        CHK_RESULT(ret);
        ret = WaveformAiCtrl_LoadProfile(wfAiCtrl, profilePath);//Loads a profile to initialize the device.
        CHK_RESULT(ret);

        IArray* chans=WaveformAiCtrl_getChannels(wfAiCtrl);
        AiChannel* myChannel_2=(AiChannel*)Array_getItem(chans,2);
        AiChannel* myChannel_3=(AiChannel*)Array_getItem(chans,3);
        AiChannel_setValueRange(myChannel_2,V_0To10);
        AiChannel_setValueRange(myChannel_3,V_0To10);

        // Step 4: Set necessary parameters
        conversion = WaveformAiCtrl_getConversion(wfAiCtrl);
        ret = Conversion_setChannelStart(conversion, startChannel);
        CHK_RESULT(ret);
        ret= Conversion_setChannelCount(conversion, channelCount);
        CHK_RESULT(ret);
        ret = Conversion_setClockRate(conversion, clockRate);
        CHK_RESULT(ret);

		record = WaveformAiCtrl_getRecord(wfAiCtrl);
        ret = Record_setSectionCount(record, sectionCount);//The 0 means setting 'streaming' mode.
        CHK_RESULT(ret);
        ret = Record_setSectionLength(record, sectionLength);
        CHK_RESULT(ret);

        // Step 5: The operation has been started.
		// We can get samples via event handlers.
        ret = WaveformAiCtrl_Prepare(wfAiCtrl);
		CHK_RESULT(ret);
		ret = WaveformAiCtrl_Start(wfAiCtrl);
        CHK_RESULT(ret);

        // Step 6: The device is acquiring data.
        //   printf("Streaming AI is in progress.\nplease wait...  any key to quit!\n\n");
        do
        {
            SLEEP(1);
        }	while (!kbhit());
      
        // step 7: Stop the operation if it is running.
        ret = WaveformAiCtrl_Stop(wfAiCtrl);
        CHK_RESULT(ret);
    }while (FALSE);

    // Step 9: Close device, release any allocated resource.
    WaveformAiCtrl_Dispose(wfAiCtrl);
    // If something wrong in this execution, print the error code on screen for tracking.
    if (BioFailed(ret))
    {
        AdxEnumToString(L"ErrorCode", (int32)ret, 256, enumString);
        printf("Some error occurred. And the last error code is 0x%X. [%ls]\n", ret, enumString);
        waitAnyKey();// wait any key to quit!
    }
    return 0;

}

//The function is used to deal with 'OverRun' Event.
void BDAQCALL OnOverRunEvent (void * sender, BfdAiEventArgs * args, void *userParam)
{
    printf("Streaming AI Overrun: offset = %d, count = %d\n", args->Offset, args->Count);
}
//The function is used to deal with 'CacheOverflow' Event.
void BDAQCALL OnCacheOverflowEvent (void * sender, BfdAiEventArgs * args, void *userParam)
{
    printf(" Streaming AI Cache Overflow: offset = %d, count = %d\n", args->Offset, args->Count);
}
//The function is used to deal with 'Stopped' Event.
void BDAQCALL OnStoppedEvent (void * sender, BfdAiEventArgs * args, void *userParam)
{
    printf("Streaming AI stopped: offset = %d, count = %d\n", args->Offset, args->Count);
}
