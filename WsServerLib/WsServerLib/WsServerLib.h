#pragma once
#pragma once
#include <stdint.h>
extern "C"
{

	// 定義 callback function 的形態
	// @ pointer: 回傳使用者設定的指標
	// @ str: 回傳的訊息
	typedef void(*HandleMessage)(void * pointer, const char *str);

	__declspec(dllexport) struct WSServer * server_new_dll(uint32_t max_connections, uint16_t port);

	__declspec(dllexport) int32_t server_listen_dll(struct WSServer * server);

	__declspec(dllexport) int32_t server_close_dll(struct WSServer * server);

	__declspec(dllexport) int32_t server_broadcaster_dll(struct WSServer * server, const char *msg);

}