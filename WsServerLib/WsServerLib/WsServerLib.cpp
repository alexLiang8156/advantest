// WsServerLib.cpp : 定義 DLL 應用程式的匯出函式。
//

#include "stdafx.h"
#include "ws_server.h"
#include "WsServerLib.h"

extern "C" {

	__declspec(dllexport) struct WSServer * server_new_dll(uint32_t max_connections, uint16_t port) {
		return server_new(max_connections, port);
	}

	__declspec(dllexport) int32_t server_listen_dll(struct WSServer * server)
	{
		return server_listen(server);
	}

	__declspec(dllexport) int32_t server_close_dll(struct WSServer * server)
	{
		return server_close(server);
	}

	__declspec(dllexport) int32_t server_broadcaster_dll(struct WSServer * server, const char *msg)
	{
		return server_broadcaster(server, msg);
	}
}