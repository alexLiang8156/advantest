設定:

## C 語言函式庫檔案位置

* ws-server\ws_server.h
* ws-server\lib\ws_server.lib  (使用 ws-server\build.bat 產生, 如果是 64 位元系統要使用 ws_server_x86_64.lib)

## WsServerLib 設定

### [偵錯] -> WsServerLib 屬性

![](屬性.PNG)

### 設定 ws_server.h 連結目錄

![](Include目錄.PNG)

### 設定 ws_server.lib 連結目錄

![](其它程式庫目錄.PNG)

### 設定要引入哪些其它函式庫

![](其它相依性.PNG)

要引入的有

* ws_server.lib
* Ws2_32.lib  (標準函式庫)
* Userenv.lib  (標準函式庫)

![](其它相依性設定.PNG)
