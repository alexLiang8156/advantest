#!/bin/bash

manualStart=/home/debian/advantest/manualStart

if [ ! -f $manualStart ]; then
    dpkg_runnung=$(pidof dpkg)
    program_runnung=$(pidof advanserver)
    UPDATE=/home/debian/advantest/update
    LOGDIR=/home/debian/advantest/log

	if [ ! -d $UPDATE ]; then
		su - debian -c "mkdir -p ${UPDATE}"
	fi	
	
	if [ ! -d $LOGDIR ]; then
	su - debian -c "mkdir -p ${LOGDIR}"
	fi

    if [ -z "${dpkg_running}" ] && [ -z "${program_running}" ]; then
        
        cd $UPDATE
        for f in `find ./advantest_*_*.deb -type f`;
        do
        	echo $f
            if [ -f $f ]; then
            	dpkg -i $f > dpkg.log
                rm -rf $f
            fi
        done
        bash /opt/advantest/scripts/run.bash &
    fi
else
    echo -e "Manual Start : "$program
fi