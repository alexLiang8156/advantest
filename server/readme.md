### 程式架構

```
                                                                                        Botnana
 IPC                                                                                    Motion  
 Handler                      WS Server + Botnana Client                                Server
+--------+          +---------------------------------------------+                    +--------+
|        |          |                                             |                    |        |
|        |  Json    |                                             |  Botnana Json API  |        |
|        +----------+----> CMD --> Translation --------------------------------------------->   |
|        |        | |                                             |                    |        |
|        |        +----> Query --> Acquire data from pool ---+    |                    |        |
|        |  Json    |                                        |    |                    |        |
|     <----------------- Response to Client <----------------+    |                    |        |
|        |  Json    |                                             |  tag|value|...     |        |
|     <----------------- Translation  <--------------Event ---+-----------------------------    |
|        |          |                                         |   |                    |        |
|        |          |    Update data pool <----Status  -------+   |                    |        |
|        |          |                                             |  Botnana Json API  |        |
|        |          |            Send command for ------------------------------------------>   |
|        |  Json    |            update data pool                 |                    |        |
|    <-------------------Event                                    |                    |        |
+--------+          +---------------------------------------------+                    +--------+        
                                                                                        
```

### 程式開發指令

* 版號： Cargo.toml 中 [package] version 
* 更新引用函式庫 `make update`
* 編譯執行檔 

```
For x86 
$ make x86-release

For arm
$ make arm-release
```
* 執行 

```
$ make run
or
$ ./target/advanserver

```

* 打包安裝檔 `./build-deb.bash`

### 程式安裝/移除的指令

以安裝檔 `advantest_0.1.0-1_armhf.deb` 為例。 

* 手動安裝 deb 檔 `$ sudo dpkg -i advantest_0.1.0-1_armhf.deb`
* 查詢 advantest 套件安裝資訊 `$ dpkg -l advantest`
* 手動移除 advantest 套件 `$ sudo dpkg -r advantest`
* 套件安裝後，更新套件的方法：

```
1. 將 deb 檔放到 /home/debian/advantest/update
2. 重新開機
```
                                                                                                     