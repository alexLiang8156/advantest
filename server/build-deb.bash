#!/bin/bash

read -p "Revision Number: ? [1]" revision
if [ "$revision" = "" ]; then
	revision=1
fi

versionNumber=$(grep '^version' ./Cargo.toml | grep -o '\".*\"' | sed 's/\"//g' )

package=advantest_$versionNumber-$revision'_armhf.deb'
echo $package

rm -rf ./build

read -p "cargo update (Y/N) ? [N]" cargoUpdate
read -p "cargo clean (Y/N) ? [N]" cargoClean

if [ "$cargoUpdate" = "Y" ] || [ "$cargoUpdate" = "y" ] ; then
	echo "cargo update"
	cargo update
fi

if [ "$cargoClean" = "Y" ] || [ "$cargoClean" = "y" ] ; then
	echo "cargo clean"
	cargo clean
fi

echo "make ARM release"
make arm-release


if [ -e ./target/advanserver ] ; then
	chmod 755 ./debian/postinst-arm
	chmod 755 ./debian/prerm-arm

	mkdir -p build/DEBIAN
	mkdir -p ./build/usr/share/doc/advantest
	mkdir -p ./build/opt/advantest/{bin,scripts,update,share}
	mkdir -p ./build/opt/advantest/share/doc
	mkdir -p ./build/etc/init.d
	mkdir -p ./build/etc/advantest
	cp ./debian/control ./build/DEBIAN

	echo 'Version: '$versionNumber >> build/DEBIAN/control
	echo 'Architecture: armhf' >> build/DEBIAN/control
	cp ./debian/prerm-arm build/DEBIAN/prerm
	cp ./debian/postinst-arm build/DEBIAN/postinst

	chmod 755 ./target/advanserver
	cp ./target/advanserver ./build/opt/advantest/bin
	cp ./debian/advantest ./build/etc/init.d
	cp ./debian/run.bash ./build/opt/advantest/scripts
	cp ./debian/run-init.bash ./build/opt/advantest/scripts
	find ./build -type d | xargs chmod 755
	fakeroot dpkg-deb --build build

	if [ ! -d release ] ; then
		mkdir release
	fi

	mv build.deb release/$package
	
	echo -e ""
	echo -e "\e[1;5;32m"./release/$package" OK !! \e[0m"
else
	echo -e ""
	echo -e "\e[1;5;31m Could not compile \e[0m"
fi
echo -e ""