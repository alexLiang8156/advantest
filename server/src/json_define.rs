use serde::{Deserialize, Serialize};
use std::collections::HashMap;

#[derive(Serialize, Deserialize)]
/// Error
struct Error {
    error: HashMap<String, String>,
}

/// Return Error json string
pub fn error(key: &str, value: &str) -> String {
    let mut error = HashMap::with_capacity(1);
    error.insert(key.to_string(), value.to_string());
    serde_json::to_string(&Error { error: error }).unwrap()
}

#[derive(Serialize, Deserialize)]
/// Option
struct Option {
    option: u32,
}

/// Return Option json string
pub fn option(option: u32) -> String {
    serde_json::to_string(&Option { option: option }).unwrap()
}

#[derive(Serialize, Deserialize)]
/// Door
struct Door {
    door: String,
}

/// Return Door json string
pub fn door(value: &str) -> String {
    serde_json::to_string(&Door {
        door: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Stop Sensor
struct StopSensor {
    stop_sensor: String,
}

/// Return Stop Sensor json string
pub fn stop_sensor(value: &str) -> String {
    serde_json::to_string(&StopSensor {
        stop_sensor: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Stp Clamped
struct StpClamped {
    stp_clamped: String,
}

/// Return Stop Sensor Json string
pub fn stp_clamped(value: &str) -> String {
    serde_json::to_string(&StpClamped {
        stp_clamped: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Initialize
struct Initialize {
    initialize: String,
}

/// Return Initialize Json string
pub fn initialize(value: &str) -> String {
    serde_json::to_string(&Initialize {
        initialize: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Stop
struct Stop {
    stop: String,
}

/// Return Initialize Json string
pub fn stop(value: &str) -> String {
    serde_json::to_string(&Stop {
        stop: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Request
struct Request {
    request: String,
}

/// Return Initialize Json string
pub fn request(value: &str) -> String {
    serde_json::to_string(&Request {
        request: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Remove
struct Remove {
    remove: String,
}

/// Return Remove json string
pub fn remove(value: &str) -> String {
    serde_json::to_string(&Remove {
        remove: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Exchange
struct Exchange {
    exchange: String,
}

/// Return Exchange json string
pub fn exchange(value: &str) -> String {
    serde_json::to_string(&Exchange {
        exchange: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Press
struct Press {
    press: String,
}

/// Return Error json string
pub fn press(value: &str) -> String {
    serde_json::to_string(&Press {
        press: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Insert
struct Insert {
    insert: String,
}

/// Return Insert json string
pub fn insert(value: &str) -> String {
    serde_json::to_string(&Insert {
        insert: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Elevator
struct Elevator {
    elev: String,
}

/// Return Elevator json string
pub fn elevator(value: &str) -> String {
    serde_json::to_string(&Elevator {
        elev: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// SetPlate
struct Setplate {
    setplate: String,
}

/// Return SetPlate json string
pub fn setplate(value: &str) -> String {
    serde_json::to_string(&Setplate {
        setplate: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Reply
struct Reply {
    reply: String,
}

/// Return Reply json string
pub fn reply(value: &str) -> String {
    serde_json::to_string(&Reply {
        reply: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Ack
struct Ack {
    ack: String,
}

/// Return Reply json string
pub fn ack(value: &str) -> String {
    serde_json::to_string(&Ack {
        ack: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// WS Reponse
struct WsResponse {
    ws: String,
}

/// Return WsResponse json string
pub fn ws_response(value: &str) -> String {
    serde_json::to_string(&WsResponse {
        ws: value.to_string(),
    })
    .unwrap()
}

#[derive(Serialize, Deserialize)]
/// Botnana
pub struct BotnanaInfo {
    pub ws_online: bool,
    pub slaves_len: u32,
    pub slaves_state: u32,
}

impl BotnanaInfo {
    /// New
    pub fn new() -> BotnanaInfo {
        BotnanaInfo {
            ws_online: false,
            slaves_len: 0,
            slaves_state: 0,
        }
    }
}

#[derive(Serialize, Deserialize)]
pub struct Botnana {
    pub botnana: BotnanaInfo,
}

impl Botnana {
    /// New
    pub fn new() -> Botnana {
        Botnana {
            botnana: BotnanaInfo::new(),
        }
    }

    /// To json String
    pub fn to_json(&self) -> String {
        serde_json::to_string(&self).unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_json() {
        assert!(error("elevator", "alarm") == r#"{"error":{"elevator":"alarm"}}"#);
        assert!(option(12) == r#"{"option":12}"#);
        assert!(door("?") == r#"{"door":"?"}"#);
        assert!(stop_sensor("?") == r#"{"stop_sensor":"?"}"#);
        assert!(stp_clamped("?") == r#"{"stp_clamped":"?"}"#);
        assert!(initialize("ready") == r#"{"initialize":"ready"}"#);
        assert!(stop("finish") == r#"{"stop":"finish"}"#);
        assert!(request("finish") == r#"{"request":"finish"}"#);
        assert!(exchange("finish") == r#"{"exchange":"finish"}"#);
        assert!(press("insert") == r#"{"press":"insert"}"#);
        assert!(remove("tray") == r#"{"remove":"tray"}"#);
        assert!(insert("finish") == r#"{"insert":"finish"}"#);
        assert!(elevator("finish") == r#"{"elev":"finish"}"#);
        assert!(setplate("clamp") == r#"{"setplate":"clamp"}"#);
        assert!(reply("ok") == r#"{"reply":"ok"}"#);
        assert!(ack("option") == r#"{"ack":"option"}"#);
        assert!(ws_response("Invalid Json String") == r#"{"ws":"Invalid Json String"}"#);
    }

}
