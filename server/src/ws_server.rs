extern crate botnanars;
use botnanars::botnana::Botnana;
use json_define;
use serde::{Deserialize, Serialize};
use serde_json;
use std::sync::{Arc, Mutex};
use ws::{util::Token, CloseCode, Handler, Handshake, Message, Result};

const EXPIRE: Token = Token(1);
const EXPIRE_MS: u64 = 30_000;

#[derive(Serialize, Deserialize)]
/// Request Json
struct Request {
    method: Option<String>,
    option: Option<u32>,
    door: Option<String>,
    insert: Option<String>,
    botnana: Option<String>,
}

/// Data Pool
pub struct DataPool {
    pub option: u32,
    pub door_opened: bool,
    pub botnana: json_define::Botnana,
}

impl DataPool {
    /// New
    pub fn new() -> DataPool {
        DataPool {
            option: 0,
            door_opened: true,
            botnana: json_define::Botnana::new(),
        }
    }
}

/// WS Server
pub struct WSServer {
    /// WS Sender
    pub out: ws::Sender,
    /// Botnana Client
    botnana: Botnana,
    /// 多個 thread 要分享的資料
    data_pool: Arc<Mutex<DataPool>>,
    /// 用來檢查 Client 是否都沒有送訊息過來
    is_timeout: bool,
}

impl WSServer {
    /// New
    pub fn new(out: ws::Sender, botnana: Botnana, data_pool: Arc<Mutex<DataPool>>) -> WSServer {
        WSServer {
            out: out,
            botnana: botnana,
            data_pool: data_pool,
            is_timeout: true,
        }
    }

    /// Option
    pub fn option(&self) -> u32 {
        self.data_pool.lock().expect("").option
    }

    /// Set Option
    pub fn set_option(&mut self, option: u32) {
        self.data_pool.lock().expect("").option = option;
    }

    /// Door opened
    pub fn door_opened(&self) -> bool {
        self.data_pool.lock().expect("").door_opened
    }

    /// botnana online
    pub fn botnana_json(&self) -> String {
        self.data_pool.lock().expect("").botnana.to_json()
    }
}

impl Handler for WSServer {
    /// On open
    fn on_open(&mut self, _: Handshake) -> Result<()> {
        self.out.send(Message::Text(json_define::ws_response(
            "WS Client is opened",
        )))?;
        // 每 EXPIRE_MS 秒檢查是否沒有任何連線活動
        self.out.timeout(EXPIRE_MS, EXPIRE)
    }

    /// On message
    fn on_message(&mut self, msg: ws::Message) -> Result<()> {
        self.is_timeout = false;
        if let ws::Message::Text(ref text) = msg {
            if let Ok(req) = serde_json::from_str::<Request>(text) {
                // method
                if let Some(method) = req.method {
                    // 回傳訊息給 client
                    self.out.send(Message::Text(json_define::ack(&method)))?;
                }

                // option
                if let Some(option) = req.option {
                    self.set_option(option);
                    // 回傳訊息給 client
                    self.out.send(Message::Text(json_define::reply("ok")))?;
                    // 轉送命令給 Botnana Motion Server
                    self.botnana.send_script_to_buffer(".verbose");
                }

                // door
                if let Some(_) = req.door {
                    let msg = if self.door_opened() {
                        json_define::door("open")
                    } else {
                        json_define::door("close")
                    };
                    self.out.send(Message::Text(msg))?;
                }

                // insert
                if let Some(insert) = req.insert {
                    self.out.send(Message::Text(json_define::ack(&insert)))?;
                }

                // botnana
                if let Some(ref req) = req.botnana {
                    match req.as_str() {
                        r#"?"# => {
                            self.out.send(Message::Text(self.botnana_json()))?;
                        }
                        r#"connect"# => {
                            self.botnana.connect();
                        }
                        _ => {}
                    }
                }

                Ok(())
            } else {
                self.out.send(Message::Text(json_define::ws_response(
                    "Invalid Json String",
                )))
            }
        } else {
            self.out.send(Message::Text(json_define::ws_response(
                "Invalid WS Message Type",
            )))
        }
    }

    /// On_timeout
    fn on_timeout(&mut self, _: Token) -> Result<()> {
        if self.is_timeout {
            self.out.close_with_reason(CloseCode::Away, "Timeout")
        } else {
            self.is_timeout = true;
            // 每 EXPIRE_MS 秒檢查是否沒有任何連線活動
            self.out.timeout(EXPIRE_MS, EXPIRE)
        }
    }

    /// On Close
    fn on_close(&mut self, _: CloseCode, reason: &str) {
        let _ = self.out.send(Message::Text(json_define::ws_response(
            &("WS Client is closed by ".to_owned() + reason),
        )));
    }
}
