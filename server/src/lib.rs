extern crate botnanars;
extern crate serde;
extern crate serde_json;
extern crate ws;

pub mod json_define;
pub mod ws_server;
