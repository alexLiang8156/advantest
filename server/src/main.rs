#[macro_use]
extern crate lazy_static;
extern crate botnanars;
extern crate server;
extern crate ws;

use botnanars::botnana;
use server::ws_server::{DataPool, WSServer};
use std::{
    ffi::CStr,
    net::{IpAddr, Ipv4Addr, SocketAddr},
    os::raw::c_char,
    str,
    sync::{
        mpsc::{channel, Sender},
        Arc, Mutex,
    },
    thread, time,
};

lazy_static! {
    static ref DATA_POOL: Arc<Mutex<DataPool>> = Arc::new(Mutex::new(DataPool::new()));
}

static mut EVENT_SENDER: Option<Mutex<Sender<(String)>>> = None;

const VERSION: &'static str = env!("CARGO_PKG_VERSION");

/// Send event message
pub fn send_event_message(msg: &str) {
    unsafe {
        let _ = EVENT_SENDER
            .as_ref()
            .unwrap()
            .lock()
            .unwrap()
            .send(msg.to_string());
    }
}

/// On botnana ws open
fn on_botnana_ws_open(_msg: *const c_char) {
    DATA_POOL.lock().unwrap().botnana.botnana.ws_online = true;
}

/// On botnana ws error
fn on_botnana_ws_error(_msg: *const c_char) {
    DATA_POOL.lock().unwrap().botnana.botnana.ws_online = false;
}

/// On botnana message
fn on_botnana_message(msg: *const c_char) {
    let _message = unsafe {
        assert!(!msg.is_null());
        str::from_utf8(CStr::from_ptr(msg).to_bytes()).unwrap()
    };
}

/// On slaves_responding
fn on_slaves_responding(msg: *const c_char) {
    let message = unsafe {
        assert!(!msg.is_null());
        str::from_utf8(CStr::from_ptr(msg).to_bytes()).unwrap()
    };
    if let Ok(x) = message.parse::<u32>() {
        DATA_POOL.lock().unwrap().botnana.botnana.slaves_len = x;
    }
}

/// On slaves state
fn on_slaves_state(msg: *const c_char) {
    let message = unsafe {
        assert!(!msg.is_null());
        str::from_utf8(CStr::from_ptr(msg).to_bytes()).unwrap()
    };
    if let Ok(x) = message.parse::<u32>() {
        DATA_POOL.lock().unwrap().botnana.botnana.slaves_state = x;
    }
}

fn main() {
    println!("Version: {}", VERSION);
    // 建造 event channel，送來通知 Server 是否因事件發生時要通知 Client
    let (event_tx, event_rx) = channel::<String>();
    unsafe {
        EVENT_SENDER = Some(Mutex::new(event_tx));
    }

    let mut botnana = botnana::Botnana::new();
    botnana.set_on_message_cb(on_botnana_message);
    botnana.set_on_open_cb(on_botnana_ws_open);
    botnana.set_on_error_cb(on_botnana_ws_error);
    botnana.times("slaves_responding", 0, on_slaves_responding);
    botnana.times("al_states", 0, on_slaves_state);

    let bna = botnana.clone();

    // Build WebSocket Server
    let websocket = ws::Builder::new()
        .with_settings(ws::Settings {
            max_connections: 10,
            ..ws::Settings::default()
        })
        .build(move |out: ws::Sender| {
            unsafe {
                let param = libc::sched_param {
                    sched_priority: libc::sched_get_priority_max(libc::SCHED_FIFO) - 5,
                };
                libc::pthread_setschedparam(libc::pthread_self(), libc::SCHED_FIFO, &param);
            }

            WSServer::new(out.clone(), bna.clone(), DATA_POOL.clone())
        })
        .unwrap();

    let handle = websocket.broadcaster();

    // WebSocket Server listen
    if let Err(e) = thread::Builder::new()
        .name("WS Listen".to_string())
        .spawn(move || {
            let address = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(0, 0, 0, 0)), 3013);

            if let Err(e) = websocket.listen(address) {
                panic!("WebSocket Server Listen Failed = {}", e);
            }
        })
    {
        panic!("Create WS Listen Thread failed = {}", e);
    }

    let out = handle.clone();
    // 建立一個 Thread 用來處理 Event
    if let Err(e) = thread::Builder::new()
        .name("Event Handle".to_string())
        .spawn(move || loop {
            // 透過 Channel，將接收到的 String 訊息送給 Client
            if let Ok(msg) = event_rx.recv() {
                let _ = out.send(ws::Message::Text(msg.clone()));
            }
        })
    {
        eprintln!("Event Handle Thread Error = {}", e);
    }

    botnana.connect();

    // 每個 Loop 要執行的動作
    loop {
        botnana.send_script_to_buffer(".ec-links");
        thread::sleep(time::Duration::from_millis(2000));
    }
}
