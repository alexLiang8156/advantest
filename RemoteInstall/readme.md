## 前言

此範例使用 Microsoft Visual Studio 2017 的 VC++ 實作遠端安裝程式到 Botnana Control 的程序。主要使用的函式庫是 [libssh](https://www.libssh.org/)

## LIBSSH

### 安裝工具 `vcpkg`

資料來源： [https://docs.microsoft.com/zh-tw/cpp/build/vcpkg?view=vs-2019](https://docs.microsoft.com/zh-tw/cpp/build/vcpkg?view=vs-2019)

從 GitHub 複製 vcpkg 存放庫：(https://github.com/Microsoft/vcpkg)[https://github.com/Microsoft/vcpkg]。 您可以下載到您偏好的任何資料夾位置。

在根資料夾中執行以下命令：

1. `bootstrap-vcpkg.bat`
2. `vcpkg integrate install`

### 利用 `vcpkg` 安裝 `libssh`

安裝指令 `vcpkg install libssh`

**注意事項：**

Visual Studio 必須安裝以下套件：

* 語言套件: 英文。
* 使用 C++ 的桌面開發。

![](C++的桌面開發.PNG)

* 個別元件。

![](VC個別元件.PNG)

## 程式流程

主程式為 `RemoteInstall/RemoteInstall.cpp`

1. 建立 ssh 連線，連線到 Botnana Control
2. 查詢 Botnana Control上的套件版本
3. 傳送安裝檔到 Botnana Control
4. 傳送安裝程序到 Botnana Control (software_update.bash, linux bash script)
5. 命令 Botnana Control 執行安裝程序 （如果未安裝就執行 `dpkg -i`，如果以安裝就將安裝到移到指定目錄）
6. 命令  Botnana Control重新開機

## 其它

* software_update.bash 必須是 UNIX 的文件格式。