﻿// RemoteInstall.cpp : 此檔案包含 'main' 函式。程式會於該處開始執行及結束執行。
//

#include "pch.h"
#include <libssh/libssh.h>
#include <libssh/sftp.h>
#include <stdlib.h>
#include <stdio.h>
#include <sys/stat.h>
#include <io.h>
#include <fcntl.h>
#include <direct.h>

// 傳送檔案到遠端電腦 
int transfer_file_to_remote(ssh_session session, const char * filename, mode_t mode)
{
	sftp_session sftp;
	sftp_file file;
	FILE *fp;

	sftp = sftp_new(session);
	if (sftp == NULL)
	{
		printf("Error allocating SFTP session: %s\n", ssh_get_error(session));
		return SSH_ERROR;
	}

	if (int rc = sftp_init(sftp) != SSH_OK)
	{
		printf("Error initializing SFTP session: %d\n", sftp_get_error(sftp));
		sftp_free(sftp);
		return rc;
	}

	file = sftp_open(sftp, filename, O_WRONLY | O_CREAT | O_TRUNC, mode);
	if (file == NULL)
	{
		printf("Can't open file for writing: %d\n", sftp_get_error(sftp));
		sftp_free(sftp);
		return SSH_ERROR;
	}

	char buffer[20480];
	if (fopen_s(&fp, filename, "rb") != 0) {
		printf("Can't open source file: %s\n", filename);
		sftp_free(sftp);
		return SSH_ERROR;
	}

	while (!feof(fp)) {
		int count = fread_s(buffer, sizeof(buffer), 1, sizeof(buffer), fp);
		int nwritten = sftp_write(file, buffer, count);
		if (nwritten != count)
		{
			printf("Can't write data to file: %d\n", sftp_get_error(sftp));
			fclose(fp);
			sftp_close(file);
			return SSH_ERROR;
		}
	}

	fclose(fp);

	if (int rc = sftp_close(file) != SSH_OK)
	{
		printf("Can't close the written file: %d\n", sftp_get_error(sftp));
		sftp_free(sftp);
		return rc;
	}

	sftp_free(sftp);
	printf("File (%s) Upload Finished\n", filename);
	return SSH_OK;
}

// 遠端電腦執行指令 
int remote_processes(ssh_session session, const char * cmd)
{
	ssh_channel channel = ssh_channel_new(session);
	char cmdbuf[512];
	char buffer[2048];
	int nbytes;

	// 命令結尾加上  echo "end-of-process"，讓命令執行完成後輸出 end-of-process，借此判斷命令已經執行完成。
	snprintf(cmdbuf, sizeof(cmdbuf), "%s ; echo \"end-of-process\"", cmd);

	if (channel == NULL) {
		return SSH_ERROR;
	}

	if (int rc = ssh_channel_open_session(channel) != SSH_OK)
	{
		ssh_channel_free(channel);
		return rc;
	}

	printf("command: %s\n", cmdbuf);

	if (int rc = ssh_channel_request_exec(channel, cmdbuf) != SSH_OK)
	{
		ssh_channel_close(channel);
		ssh_channel_free(channel);
		return rc;
	}

	int end = 0;
	while (end == 0)
	{
		nbytes = ssh_channel_read(channel, buffer, sizeof(buffer), 0);
		if (nbytes > 0) {
			if (strstr(buffer, "end-of-process") != NULL) {
				end = 1;
			}
			else {
				printf("%s", buffer);
			}
		}
		Sleep(100);
	}

	ssh_channel_send_eof(channel);
	ssh_channel_close(channel);
	ssh_channel_free(channel);
	return SSH_OK;
}

int main(int argc, char *argv[])
{
	ssh_session botnana_session = ssh_new();
	char cmdbuf[256];

	// 檢查引數
	if (argc < 4) {
		// argv[1]: IP
		// argv[2]: 套件名稱
		// argv[3]: 安裝檔名稱
		printf("Command Format: ConsoleSSH \"192.168.7.2\" \"advantest\"  \"advantest_0.1.0-1_armhf.deb\"\n");
		return -1;
	}

	if (botnana_session == NULL) {
		printf("Can't create session\n");
		return -1;
	}

	// 等待 5 秒，等待遠端電腦開機完成
	printf("Wait remote computer to start\n");
	Sleep(5000);

	ssh_options_set(botnana_session, SSH_OPTIONS_HOST, argv[1]);
	if (ssh_connect(botnana_session) != SSH_OK)
	{
		fprintf(stderr, "Error connecting to %s: %s\n", argv[1], ssh_get_error(botnana_session));
		ssh_free(botnana_session);
		return -1;
	}

	// Set user and password
	if (ssh_userauth_password(botnana_session, "debian", "temppwd") != SSH_AUTH_SUCCESS)
	{
		printf("Error authenticating with password: %s\n", ssh_get_error(botnana_session));
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}

	// 讀取套件版本
	snprintf(cmdbuf, sizeof(cmdbuf), "dpkg -s %s", argv[2]);
	if (remote_processes(botnana_session, cmdbuf) < 0) {
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}

	// 傳送安裝檔
	if (transfer_file_to_remote(botnana_session, argv[3], 0644) < 0)
	{
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}
	// 傳送安裝程序
	if (transfer_file_to_remote(botnana_session, "software_update.bash", 0755) < 0)
	{
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}
	// 執行安裝程序
	if (remote_processes(botnana_session, "./software_update.bash") < 0) {
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}

	// 移除安裝程序
	if (remote_processes(botnana_session, "rm -rf software_update.bash") < 0) {
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}

	// 重新開機的命令
	if (remote_processes(botnana_session, "echo temppwd | sudo -S shutdown -r now") < 0) {
		ssh_disconnect(botnana_session);
		ssh_free(botnana_session);
		return -1;
	}

	printf("Finished!!\n");
	return 0;
}

// 執行程式: Ctrl + F5 或 [偵錯] > [啟動但不偵錯] 功能表
// 偵錯程式: F5 或 [偵錯] > [啟動偵錯] 功能表

// 開始使用的秘訣: 
//   1. 使用 [方案總管] 視窗，新增/管理檔案
//   2. 使用 [Team Explorer] 視窗，連線到原始檔控制
//   3. 使用 [輸出] 視窗，參閱組建輸出與其他訊息
//   4. 使用 [錯誤清單] 視窗，檢視錯誤
//   5. 前往 [專案] > [新增項目]，建立新的程式碼檔案，或是前往 [專案] > [新增現有項目]，將現有程式碼檔案新增至專案
//   6. 之後要再次開啟此專案時，請前往 [檔案] > [開啟] > [專案]，然後選取 .sln 檔案
