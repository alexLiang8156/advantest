#!/bin/bash
# 注意此檔案要用 UNIX 格式
# 套件名稱 
package=advantest
# 用套件版號檢查是否已經安裝過
version=$(dpkg -s $package | grep 'Version:')
echo $version

# 尋找套件的安裝檔
for f in `find ${package}_*_*.deb -type f`;
do
    echo $f
    if [ -f $f ]; then
        if [ "$version" = "" ]; then
            # 沒有安裝過套件的話，就要安裝
            echo "Install "$f
            echo "temppwd" | sudo -S dpkg -i $f > dpkg.log
            rm -rf $f
        else
            # 有安裝過的話就移到指定的目錄
            echo "move file to /home/debian/advantest/update" 
            mv $f /home/debian/advantest/update
        fi      
    fi
done
   
